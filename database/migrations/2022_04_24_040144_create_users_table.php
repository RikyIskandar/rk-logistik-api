<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('identitas_id');
            $table->char('gender', 1)->comment('0: female, 1: male')->default(0);
            $table->string('alamat');
            $table->string('foto');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('nomor_hp');
            $table->string('api_token', 40);
            $table->char('role', 1)->comment('0: admin, 1: driver, 2: users');
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
