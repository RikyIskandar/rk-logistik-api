<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('tracking_number')->unique();
            $table->unsignedBigInteger('customer_id');
            $table->string('sender_name');
            $table->string('sender_phone');
            $table->string('sender_address');
            $table->string('delivery_name');
            $table->string('delivery_phone');
            $table->string('delivery_address');
            $table->unsignedBigInteger('category_id');
            $table->integer('biaya_pengiriman');
            $table->string('harga_barang');
            $table->boolean('asuransi')->default(false);
            $table->unsignedBigInteger('user_id');
            $table->integer('biaya_user');
            $table->string('catatan')->nullable();
            $table->string('foto_pengiriman');
            $table->string('foto_terkirim');
            $table->char('status', 1)->comment('0: baru, 1: pengiriman, 2: selesai, 3: keluhan, 4: batal');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
